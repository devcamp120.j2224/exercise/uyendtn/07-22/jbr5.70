package com.devcamp.s50.jbr570.api.model;

public class Animal {
    String name;

    public Animal(String name) {
        this.name = name;
    }
    public String toString() {
        return "Animal[name = " + name + "]";
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
