package com.devcamp.s50.jbr570.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.jbr570.api.model.Animal;
import com.devcamp.s50.jbr570.api.model.Cat;
import com.devcamp.s50.jbr570.api.model.Dog;
import com.devcamp.s50.jbr570.api.service.AnimalService;

@RestController
@RequestMapping("/")
@CrossOrigin(value="*",maxAge = -1)
public class AnimalController {
    @Autowired
    private AnimalService animalService;
    
    @GetMapping("/animals")
    public List<Animal> getAnimal() {
        return animalService.getAnimalList();
    }

    @GetMapping("/cats")
    public List<Animal> getCats() {
        List<Animal> animals = animalService.getAnimalList();
        List<Animal> cats = new ArrayList<Animal>();
        for(Animal animal: animals){
            if (animal instanceof Cat){
                cats.add(animal);
            }
        }
       return cats;
    }

    @GetMapping("/dogs")
    public List<Animal> getDogs() {
        List<Animal> animals = animalService.getAnimalList();
        List<Animal> dogs = new ArrayList<Animal>();

        for(Animal animal: animals){
            if (animal instanceof Dog){
                dogs.add(animal);
            }
        }
       return dogs;
    }
}
