package com.devcamp.s50.jbr570.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.s50.jbr570.api.model.Animal;
import com.devcamp.s50.jbr570.api.model.Cat;
import com.devcamp.s50.jbr570.api.model.Dog;

@Service
public class AnimalService {
    Cat cat1 = new Cat("Kitty");
    Cat cat2 = new Cat("Mimi");
    Dog dog1 = new Dog("Lulu");
    Dog dog2 = new Dog("Cogi");

    public List<Animal> getAnimalList() {
        List<Animal> animalList = new ArrayList<>();
        animalList.add(cat1);
        animalList.add(cat2);
        animalList.add(dog1);
        animalList.add(dog2);

        return animalList;
    }
    public List<Animal> getCats() {
        List<Animal> catList = new ArrayList<>();
        catList.add(cat1);
        catList.add(cat2);
        return catList;
    }
    public List<Animal> getDogs() {
        List<Animal> dogList = new ArrayList<>();
        dogList.add(dog1);
        dogList.add(dog2);
        return dogList;
    }
}
